package com.blackid.recepie_app;

import java.util.ArrayList;

import com.blackid.bean.CategoryItem;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class subcategory extends Activity 
{
	
	SubviewAdapter subadapter;
	ArrayList<CategoryItem> recepieitem;
	ImageButton ibnewrefresh,ibnewback;
	TextView tvMainHead;
	ListView lv2 ;
	String main;  
	
		@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			super.onBackPressed();
			Intent iback = new Intent(getApplicationContext(), MainActivity.class);
			iback.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(iback);
			//subcategory.this.finish();
		
			}

			@SuppressWarnings("unchecked")
			@Override
		protected void onCreate(Bundle savedInstanceState) {
				// TODO Auto-generated method stub
			   super.onCreate(savedInstanceState);
			   setContentView(R.layout.appe_main);
			
			//	rnew                =  new ArrayList<recepielist>();
				lv2                = (ListView) findViewById(R.id.lstappet);
			    tvMainHead          = (TextView) findViewById(R.id.txtappet);
				ibnewrefresh        = (ImageButton) findViewById(R.id.imgrefreshnew);
				ibnewback           =  (ImageButton) findViewById(R.id.imgbacknew);
				
				if(getIntent().getExtras() != null){
					Bundle b            =  getIntent().getExtras();
				    recepieitem         =  (ArrayList<CategoryItem>) b.getSerializable("subcategoryitems");
					subadapter          =  new SubviewAdapter(getApplicationContext(), recepieitem);
					lv2.setAdapter(subadapter);
					main				=  (String) b.getSerializable("val");
					tvMainHead.setText(main);
				}
				
				
			//	Bundle bnew  = new Bundle();
				//bnew         = getIntent().getExtras();
				//	if(bnew != null)
					//{
					//	String main  = (String) bnew.getSerializable("val");
					//	tvMainHead.setText(main);
					//}
				
			lv2.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View v,
						int pos, long id) {
					// TODO Auto-generated method stub
				
						Intent i    = new Intent(subcategory.this,fullmenuitem.class);
					    Bundle b1   = new Bundle();
					    b1.putSerializable("key1", recepieitem.get(pos));
						b1.putSerializable("subcategoryitems", recepieitem);
						b1.putSerializable("val", main);
						i.putExtras(b1);
						startActivity(i);
						subcategory.this.finish();
				
				}
			});
			
			ibnewrefresh.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					Handler hnew = new Handler();
					hnew.postDelayed(new Runnable()
					{
						   @Override
						   public void run() {
							// TODO Auto-generated method stub
							
							   Toast.makeText(getApplicationContext(), "Page Refresh", Toast.LENGTH_LONG).show();
						}
					}, 1000);
				}
			});
			
			ibnewback.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
						
						Intent iback = new Intent(getApplicationContext(), MainActivity.class);
						iback.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(iback);
						subcategory.this.finish();
				
					}
				});
			}
}
