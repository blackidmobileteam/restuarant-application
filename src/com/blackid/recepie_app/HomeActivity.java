package com.blackid.recepie_app;

import java.util.ArrayList;

import com.blackid.bean.HomeScreenList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;

public class HomeActivity extends Activity {
	
	ListView lvhome;
	HomeListAdapter homeadapter;
	ArrayList<HomeScreenList> homeListItems;
	ImageButton icontact;

		@Override
		protected void onCreate(Bundle savedInstanceState) 
		{
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			setContentView(R.layout.home_screen);
			ImageButton icontact  = (ImageButton) findViewById(R.id.imgcontact);
			
			
			lvhome                =  (ListView) findViewById(R.id.listhome);
			homeListItems         = new ArrayList<HomeScreenList>();
			
			HomeScreenList h1     = new HomeScreenList("Menu List");
			homeListItems.add(h1);
			
			HomeScreenList h2     = new HomeScreenList("Your order");
			homeListItems.add(h2);
			
			
			HomeScreenList h3     = new HomeScreenList("Reservation");
			homeListItems.add(h3);
			
			homeadapter           = new HomeListAdapter(getApplicationContext(), homeListItems);
			lvhome.setAdapter(homeadapter);
			
			lvhome.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> l, View v,
					int position, long id)
			{
				// TODO Auto-generated method stub
				
				if(position == 0)
				{
					Intent i = new Intent(getApplicationContext(), MainActivity.class);
					startActivity(i);
				}
				else if(position == 1)
				{
					
				}
				else if(position == 2)
				{
					
					Intent i1 = new Intent(getApplicationContext(), ReservationActivity.class);
					startActivity(i1);
					
				}
				
			
			}
		});
			
			
			
			icontact.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					
					Intent i = new Intent(getApplicationContext(), Contact.class);
					startActivity(i);
					
				}
			});
			
			}
	
}
