package com.blackid.recepie_app;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;

public class ExtraActivity extends FragmentActivity 
{

	static final LatLng HAMBURG = new LatLng(53.558, 9.927);
	static final LatLng KIEL = new LatLng(53.551, 9.993);
	private GoogleMap map;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.extra);
	        map = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
	     
		    Marker marker = map.addMarker(new MarkerOptions()
		     .position(HAMBURG)
		     .title("San Francisco"));
	     //.snippet("Population: 776733"));
	   
	  }
	
	  @Override
	  public boolean onCreateOptionsMenu(Menu menu) {
	    getMenuInflater().inflate(R.menu.main, menu);
	    return true;
	  }
	
	
	
}
