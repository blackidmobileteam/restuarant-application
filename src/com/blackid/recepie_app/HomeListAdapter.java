package com.blackid.recepie_app;

import java.util.ArrayList;

import com.blackid.bean.HomeScreenList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class HomeListAdapter extends BaseAdapter {
	
	
		Context context;
		ArrayList<HomeScreenList> homeItems;
		LayoutInflater inflate;
		
				public HomeListAdapter(Context con,ArrayList<HomeScreenList> items)
				{
					
					context        =   con;
					homeItems      =   items;
					inflate        =   (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					
				}
		
	
				@Override
				public int getCount() {
					// TODO Auto-generated method stub
					return homeItems.size();
				}
			
				@Override
				public Object getItem(int position) {
					// TODO Auto-generated method stub
					return homeItems.get(position);
				}
			
				@Override
				public long getItemId(int arg0) {
					// TODO Auto-generated method stub
					return 0;
				}
			
				@Override
				public View getView(int position, View convertview, ViewGroup parent) {
					// TODO Auto-generated method stub
					
					
					convertview           =  inflate.inflate(R.layout.home_list_view, parent, false);
					TextView tvhome       =  (TextView) convertview.findViewById(R.id.txthomehead);
					
					tvhome.setText(homeItems.get(position).getHomeListItem());
					return convertview;
				}

}
