package com.blackid.recepie_app;


import com.blackid.bean.CategoryItem;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class fullmenuitem extends Activity{
	
	ImageView ivmain;
	TextView tvhead,tvprice,tvppl,tvdetails;
	ImageButton ibnew;
	CategoryItem careitem;
	Bundle b1;
	
		@Override
		public void onBackPressed()
		{
			// TODO Auto-generated method stub
			super.onBackPressed();
			Intent ilast = new Intent(fullmenuitem.this, subcategory.class);
			ilast.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			ilast.putExtras(b1);
			startActivity(ilast);
			fullmenuitem.this.finish();
		}

		@Override
		protected void onCreate(Bundle savedInstanceState)
		{
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			setContentView(R.layout.last_app);
			
			ivmain    = (ImageView) findViewById(R.id.imgmain);
			tvhead    = (TextView) findViewById(R.id.txthead);
			tvprice   = (TextView) findViewById(R.id.txtprice);
			tvppl     = (TextView) findViewById(R.id.txtppl);
			tvdetails = (TextView) findViewById(R.id.txtdetail);
			ibnew     =  (ImageButton) findViewById(R.id.imglastback);
		
			careitem  = new CategoryItem();
			b1        = new Bundle();
			b1        = getIntent().getExtras();
			careitem  = (CategoryItem) b1.getSerializable("key1");
            
			tvhead.setText(careitem.getItemName());
			tvprice.setText(Float.toString(careitem.getItemPrice()));
			ivmain.setImageResource(careitem.getSubimgid1());
			tvppl.setText(careitem.getItemPPl());
			tvdetails.setText(careitem.getItemDetails());
				
			ibnew.setOnClickListener(new OnClickListener() 
			{
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					Intent ilast = new Intent(fullmenuitem.this, subcategory.class);
					ilast.putExtras(b1);
					startActivity(ilast);
					fullmenuitem.this.finish();
				
				}
			});
		
		}
	
}
