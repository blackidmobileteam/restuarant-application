package com.blackid.recepie_app;

import java.util.ArrayList;

import com.blackid.bean.CategoryItem;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
    ListView lv1;
    ArrayList<recepielist> recepieitem;
    ListviewCategoryAdapter cateadapter;
    ImageButton ibrefresh,imgback;
    
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
			lv1         = (ListView) findViewById(R.id.lstmain);
			ibrefresh   = (ImageButton) findViewById(R.id.imgrefresh);
			imgback     =  (ImageButton) findViewById(R.id.imgback);
			recepieitem = new ArrayList<recepielist>();
			
			recepielist r1 = new recepielist("Appteziers", R.drawable.appleter);
			ArrayList<CategoryItem> l1 = new ArrayList<CategoryItem>();
			CategoryItem i1 = new CategoryItem("Vegeterian Nachos", 7.88f,R.drawable.fast_food1,"Serve for 2 people","An Intent object carries information that the Android system uses to determine which component to start (such as the exact component name or component category that should receive the intent), plus information ","Appetizer");
			CategoryItem i2 = new CategoryItem("Pumpkin Quasedilla", 8.88f ,R.drawable.pumpkin_ques,"Serve for 2 people","EFGH","Appetizer");
			CategoryItem i3 = new CategoryItem("Fired Clab Claws", 7.88f,R.drawable.fried,"Serve for 2 people","IJKL","Appetizer");
			CategoryItem i4 = new CategoryItem("Chicken Wings", 7.88f,R.drawable.chicken_wings,"Serve for 2 people","MNOP","Appetizer");
			
			l1.add(i1);
			l1.add(i2);
			l1.add(i3);
			l1.add(i4);
			
			r1.setItemList(l1);
			recepieitem.add(r1);
		
			recepielist r2  = new recepielist("Salad", R.drawable.salad);
			ArrayList<CategoryItem> l2 = new ArrayList<CategoryItem>();
			CategoryItem c2 = new CategoryItem("Greek Salad", 7.88f,R.drawable.greek_salad,"Serve for 2 people","ABCD","Salad");
			CategoryItem c3 = new CategoryItem("Chicken Salad", 7.88f,R.drawable.greek_salad,"Serve for 2 people","ABCD","Salad");

			l2.add(c2);
			l2.add(c3);
			
			r2.setItemList(l2);
			recepieitem.add(r2);
			
			recepielist r3    = new recepielist("Pasta", R.drawable.pasta);
			ArrayList<CategoryItem> l3 = new ArrayList<CategoryItem>();
			CategoryItem p1 = new CategoryItem("Raivoli Pasta with tomato soup", 7.88f,R.drawable.pasta_1,"Serve for 2 people","ABCD","Pasta");
			CategoryItem p2 = new CategoryItem("Pasta with olives and parsely", 7.88f,R.drawable.pasta_2,"Serve for 2 people","ABCD","Pasta");
			
			l3.add(p1);
			l3.add(p2);
			
			r3.setItemList(l3);
			recepieitem.add(r3);

			cateadapter = new ListviewCategoryAdapter(MainActivity.this, recepieitem);
			lv1.setAdapter(cateadapter);
		
			lv1.setOnItemClickListener(new OnItemClickListener() {
	
			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long id) {
				// TODO Auto-generated method stub
			
					 
					Intent i = new Intent(MainActivity.this, subcategory.class);
					Bundle b = new Bundle();
					//Bundle b1= new Bundle();
					b.putSerializable("subcategoryitems", recepieitem.get(position).itemList);
					b.putSerializable("val", recepieitem.get(position).getStext());
					
					i.putExtras(b);
					//i.putExtras(b1);
					startActivity(i);
					MainActivity.this.finish();
		}
	});
		
		
			ibrefresh.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) 
				{
					// TODO Auto-generated method stub
					
					Handler h = new Handler();
					h.postDelayed(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							
							Toast.makeText(MainActivity.this, "Page Refresh", Toast.LENGTH_LONG).show();
						}
					}, 1000);
				
				}
			});
			
			imgback.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					MainActivity.this.finish();
				}
			});
				
			

	}

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.main, menu);
			return true;
		}

}
