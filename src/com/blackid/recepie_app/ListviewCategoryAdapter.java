package com.blackid.recepie_app;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListviewCategoryAdapter extends BaseAdapter
{
	
	      Context context;
	      ArrayList<recepielist> recepiedata;
	      LayoutInflater inflate;
	      
		      public ListviewCategoryAdapter(Context con,ArrayList<recepielist> items)
		      {
		    	  
		    	  context           =  con;
		    	  recepiedata       =  items;
		    	  inflate           =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		    	
		      }
	
				@Override
				public int getCount() {
					// TODO Auto-generated method stub
					return recepiedata.size();
				}
			
				@Override
				public Object getItem(int position) {
					// TODO Auto-generated method stub
					return recepiedata.get(position);
				}
			
				@Override
				public long getItemId(int arg0) {
					// TODO Auto-generated method stub
					return 0;
				}
			
				@Override
				public View getView(int position, View convertview, ViewGroup parent) {
					// TODO Auto-generated method stub
					
					
					convertview         = inflate.inflate(R.layout.category_view, parent, false);
					ImageView iv1       = (ImageView) convertview.findViewById(R.id.imageView1);
					TextView  tvcategory=(TextView) convertview.findViewById(R.id.textView1);
					
					iv1.setImageResource(recepiedata.get(position).getImgid1());
					tvcategory.setText(recepiedata.get(position).getStext());
				
					return convertview
							;
				}

}
