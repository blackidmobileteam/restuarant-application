package com.blackid.recepie_app;

import java.util.ArrayList;

import com.blackid.bean.CategoryItem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SubviewAdapter extends BaseAdapter {
	
		Context context;
		ArrayList<CategoryItem> categorydata;
		LayoutInflater inflate;
			
			public SubviewAdapter (Context con,ArrayList<CategoryItem> items)
			{
				
				context      = con;
				categorydata = items;
				inflate      = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			}
			
		
			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return categorydata.size();
			}
		
			@Override
			public Object getItem(int position) {
				// TODO Auto-generated method stub
				return categorydata.get(position);
			}
		
			@Override
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub
				return 0;
			}
		
			@Override
			public View getView(int position, View convertview, ViewGroup parent) {
				// TODO Auto-generated method stub
				
				
				convertview       = inflate.inflate(R.layout.app_view, parent , false);
				ImageView iv1     = (ImageView) convertview.findViewById(R.id.imgapp);
				TextView  tvhead  = (TextView)  convertview.findViewById(R.id.txtapphead);
				TextView  tvprice = (TextView) convertview.findViewById(R.id.txtappprice);
				
				iv1.setImageResource(categorydata.get(position).getSubimgid1());
				tvhead.setText(categorydata.get(position).getItemName());
				tvprice.setText(Float.toString(categorydata.get(position).getItemPrice()));
				
				return convertview;
			}

}
