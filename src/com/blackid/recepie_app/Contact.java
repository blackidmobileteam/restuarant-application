package com.blackid.recepie_app;

import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;


public class Contact extends  FragmentActivity
{

	ImageButton iconback;
	Button      callus,emailus;
	GoogleMap   map;
	static final LatLng HAMBURG = new LatLng(53.558, 9.927);
	static final LatLng KIEL = new LatLng(53.551, 9.993);
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.extra);
			
		 map        = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		 Marker m   = map.addMarker(new MarkerOptions().position(HAMBURG)
					 .title("You are here"));
		 
		 
		
		iconback    =  (ImageButton) findViewById(R.id.imgconback);
		callus      =  (Button) findViewById(R.id.btncallus);
		emailus     =  (Button) findViewById(R.id.btnemailus);
		
		iconback.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Contact.this.finish();
				
			}
		});
		
		
		 callus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent  i = new Intent(Intent.ACTION_CALL);
				i.setData(Uri.parse("tel:09999"));
				startActivity(i);
				
			}
		});
		 
		 
		 emailus.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				
				Intent i  = new Intent(Intent.ACTION_SEND);
				i.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
				i.putExtra(Intent.EXTRA_EMAIL, "abc@gmail.com");
				startActivity(i);
				
			}
		});
	
	}
	
	
	

}
