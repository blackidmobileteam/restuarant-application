package com.blackid.recepie_app;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

public class ReservationActivity extends FragmentActivity
{
	ImageButton irev;
	TextView    tv_date;
	TextView    tv_time;
	int r_day,r_month,r_year;
	int r_hour,r_minute;
	static final int DATE_PICKER_ID = 112;
	static final int TIME_PICKER_ID = 113;
	int m_day,m_month,m_year,m_hour,m_minute;
	
	 	public ReservationActivity()
	 	{
	 		Calendar c = Calendar.getInstance();
			m_day      = c.get(Calendar.DAY_OF_MONTH);
			m_month	   = c.get(Calendar.MONTH);
			m_year	   = c.get(Calendar.YEAR);
			m_hour	   = c.get(Calendar.HOUR);
			m_minute   = c.get(Calendar.MINUTE);
	 	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reservation_page);

		irev    = (ImageButton) findViewById(R.id.imgrevback);
		tv_date = (TextView) findViewById(R.id.txtdate);
		tv_time = (TextView) findViewById(R.id.txttime);
		
			irev.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					ReservationActivity.this.finish();
				
				}
			});
			
			
		tv_date.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showDialog(DATE_PICKER_ID);
			
				
		}

			
		});
		
		
		tv_time.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showDialog(TIME_PICKER_ID);
			
			
			
			}
		});
	
}
	
	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id) {
		// TODO Auto-generated method stub
		switch(id){
		
		case DATE_PICKER_ID:
			return new DatePickerDialog(this, pickerListner, m_year, m_month, m_day);
			
		
		case TIME_PICKER_ID:
			return new TimePickerDialog(this, timeListner, m_hour, m_minute, true);
		}
		
		return null;
	}
	

			DatePickerDialog.OnDateSetListener pickerListner = new OnDateSetListener() {
				
				@Override
				public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
					// TODO Auto-generated method stub
				
					r_day     = dayOfMonth;
					r_month   = monthOfYear;
					r_year 	  = year;
					
					tv_date.setText(new StringBuilder().append(r_month+1).append("/").append(r_day).append("/").append(r_year));
			
				}
			};
			
			
			TimePickerDialog.OnTimeSetListener timeListner = new TimePickerDialog.OnTimeSetListener() {
				
				@Override
				public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
					// TODO Auto-generated method stub
					
					r_minute = minute;
					r_hour   = hourOfDay;
					
					tv_time.setText(new StringBuilder().append(r_hour).append(":").append(r_minute));
					
				}
			};
	
	
	

}
