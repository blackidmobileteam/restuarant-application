package com.blackid.bean;

import java.io.Serializable;


public class CategoryItem implements Serializable{
	
	/**
	 * 
	 */
			private static final long serialVersionUID = -5747106961760156910L;
			private String  itemName;
			private String  itemMainHead;
			private float   itemPrice;
			private int     subimgid1;
			private String  itemPPl;
			private String  itemDetails;
			public String getItemPPl() {
				return itemPPl;
			}

			public void setItemPPl(String itemPPl) {
				this.itemPPl = itemPPl;
			}

			public String getItemDetails() {
				return itemDetails;
			}

			public void setItemDetails(String itemDetails) {
				this.itemDetails = itemDetails;
			}

			
			
			public int getSubimgid1() {
				return subimgid1;
			}
		
			public void setSubimgid1(int subimgid1) {
				this.subimgid1 = subimgid1;
			}
		
			public static long getSerialversionuid() {
				return serialVersionUID;
			}
		
			public CategoryItem(){
				
			}
			
			public CategoryItem(String name,float price,int id1,String ppl,String details,String MainHead){
				itemName	= name;
				itemPrice	= price;
				subimgid1   = id1;
				itemPPl     = ppl;
				itemDetails = details;
				itemMainHead= MainHead;
			}
		
			public String getItemMainHead() {
				return itemMainHead;
			}

			public void setItemMainHead(String itemMainHead) {
				this.itemMainHead = itemMainHead;
			}

			public String getItemName() {
				return itemName;
			}
		
			public void setItemName(String itemName) {
				this.itemName = itemName;
			}
		
			public float getItemPrice() {
				return itemPrice;
			}
		
			public void setItemPrice(float itemPrice) {
				this.itemPrice = itemPrice;
			}
			
}
